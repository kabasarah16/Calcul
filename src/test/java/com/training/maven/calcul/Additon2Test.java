package com.training.maven.calcul;

import com.training.maven.calcul.Addition2;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple Addition.
 */
public class Additon2Test 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public Additon2Test( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test addition2()
    {
        return new TestSuite( Additon2Test.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testAddition()
    {
    	Addition2 add2 = new Addition2();
    	add2.setA(5);
    	add2.setB(2);
    	assertEquals(add2.additionner(), 7);
    }
    
    
}
