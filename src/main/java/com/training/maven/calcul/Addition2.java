package com.training.maven.calcul;

import java.util.Scanner;

public class Addition2 {

	private int a,b;
	
	
	public int getA() {
		return a;
	}


	public void setA(int a) {
		this.a = a;
	}


	public int getB() {
		return b;
	}


	public void setB(int b) {
		this.b = b;
	}
	
	public int additionner() {	
		return a+b;
	}

}
