package com.training.maven.calcul;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		/*
		 * Addition 1
		 */
		Addition ad = new Addition(7, 3);
		System.out.println(ad.getA() + " + " + ad.getB() + " = " + ad.somme());
		/*
		 * Addition 2
		 */
		Scanner sc = new Scanner(System.in);
		Addition2 ad2 = new Addition2();

		System.out.println("Un premier nombre");
		ad2.setA(sc.nextInt());
		System.out.println("Un second nombre");
		ad2.setB(sc.nextInt());

		int somme = ad2.getA() + ad2.getB();

		System.out.println("la somme de " + ad2.getA() + " et " + ad2.getB() + " est : " + somme);

	}

}
