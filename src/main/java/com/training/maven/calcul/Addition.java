package com.training.maven.calcul;

public class Addition {

	private int a,b;
	
	public Addition(int nb1, int nb2) {
		this.a = nb1;
		this.b = nb2;
	}
	
	public int somme() {
		return (this.a + this.b);
	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

}
